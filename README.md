# RateShift2020

A reanalysis of Pupko and Galtier's 2002 article on detecting rate shifts in Primate mitochondrial genomes.


Retrieve sequences from HOGENOM. First search family name using sp=Mammalia ET o=Mitochondrion ET k=cox1
Note the family name (CLU_XXX) and then search sequences using sp=Mammalia and setting the family name to the noted one. Save as mase.

List of families used:

| gene | HOGENOM family |
|------|----------------|
| atp6 | CLU_041018_0_2 |
| atp8 |  CLU_2811757_0_0 |
| cox1 | CLU_011899_7_3 |
| cox2 | CLU_036876_2_3 |
| cox3 | CLU_044071_0_0 |
| nd1 | CLU_015134_0_1 |
| nd2 | CLU_007100_1_3 |
| nd3 | CLU_119549_3_1 |
| nd4 | CLU_007100_4_0 |
| nd4L | CLU_182394_0_0 |
| nd5 | CLU_007100_6_0 |
| nd6 | CLU_129718_0_0 |
| cytb | CLU_031114_3_0 |

Align in seaview with clustalo. Then build a phylogeny with PhyML, default option but BEST of NNI and SPR.

```bash
bpprateshift param=rateshift.bpp DATA=atp6 foreground_branches=25-36
bpprateshift param=rateshift.bpp DATA=atp8 foreground_branches=43-53
bpprateshift param=rateshift.bpp DATA=cox1 foreground_branches=0-10
bpprateshift param=rateshift.bpp DATA=cox2 foreground_branches=0-9,56
bpprateshift param=rateshift.bpp DATA=cox3 foreground_branches=0-10
bpprateshift param=rateshift.bpp DATA=cytb foreground_branches=0-9,56
bpprateshift param=rateshift.bpp DATA=nd1 foreground_branches=0-10 #Note: had to remove two sequences with lots of gaps and ambiguous alignment.
bpprateshift param=rateshift.bpp DATA=nd2 foreground_branches=42-52
bpprateshift param=rateshift.bpp DATA=nd3 foreground_branches=0-10
bpprateshift param=rateshift.bpp DATA=nd4 foreground_branches=0-10
bpprateshift param=rateshift.bpp DATA=nd4L foreground_branches=0-18 #Note: removed 2 redundant, incomplete sequences.
bpprateshift param=rateshift.bpp DATA=nd5 foreground_branches=13-23
bpprateshift param=rateshift.bpp DATA=nd6 foreground_branches=43-53
```
